# AsyncLogger
 - AsyncLogger is a tiny demo C++ project. 
 - AsyncLogger provides multi-threaded logging with arbitrary data type/output stream support (stdout, stderr, file, string buffer).
 - It is intended for intensively logging applications, where storage media may be a bottle-neck: log strings are cached with in-memory queue, and dedicated thread asynchronously dumps the queue to the target stream.


# Dependencies
 - C++ Standard Library;
 - gtest - Google unit test framework;


# Deliveries
 - logger/logger.h - Logger class implementation;
 - LoggerTest.cpp - unit tests for the Logger class.
 - LoggerTest.sln - MS Visual Studio 2014 solution for testing Logger class implementation;
