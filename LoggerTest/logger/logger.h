#ifndef LOGGER_H
#define LOGGER_H

#define LOGGER_CACHE_LIMIT int(1000)

using namespace std;
#include <iostream>
#include <queue>
#include <mutex>
#include <thread>

template <typename T> class Logger {
	T &m_targetStream;
	queue<string> m_cache;
	mutex m_cacheMutex;
	mutex m_dumpMutex;
	condition_variable m_condVar;
	thread* m_pDumperThread;
	bool m_done;
	bool m_cacheUpdated;

protected:
	thread* StartDumperThread() {
		return new thread([=] { DumperThreadProc(this); });
	};

	bool PushMessageToCache(const string &msg)
	{
		unique_lock<mutex> cacheLock(m_cacheMutex);
		if (m_done || m_cache.size() >= LOGGER_CACHE_LIMIT) {
			return false;
		}
		m_cache.push(msg);
		return true;
	}

	void SendCacheUpdateNotification()
	{
		lock_guard<mutex> dumpLock(m_dumpMutex);
		m_cacheUpdated = true;
		m_condVar.notify_one();
	}

	void WaitCacheUpdateNotification()
	{
		unique_lock<mutex> dumpLock(m_dumpMutex);
		m_condVar.wait(dumpLock, [=] { return (!m_cache.empty() || m_cacheUpdated); });
		m_cacheUpdated = false;
	};

	bool PopMessageFromCache(string &msg)
	{
		unique_lock<mutex> cacheLock(m_cacheMutex);
		if (m_cache.empty())
			return false;
		msg.assign(m_cache.front());
		m_cache.pop();
		return true;
	};

	void Dump(const string msg) {
		m_targetStream << msg << endl;
	}

	bool Done()
	{
		unique_lock<mutex> cacheLock(m_cacheMutex);
		if (m_cache.empty() && m_done)
			return true;
		return false;
	};

public:
	Logger(T &ts) : m_targetStream(ts), m_pDumperThread(nullptr), m_done(false) {
		lock_guard<mutex> dumpLock(m_dumpMutex);
		m_cacheUpdated = false;
		m_pDumperThread = StartDumperThread();
	}

	Logger(T *pts) : m_targetStream(*pts), m_pDumperThread(nullptr), m_done(false) {
		lock_guard<mutex> dumpLock(m_dumpMutex);
		m_cacheUpdated = false;
		m_pDumperThread = StartDumperThread();
	}

	~Logger() {
		Deinit();
	};

	void Deinit() {
		m_done = true;
		if (!m_pDumperThread)
			return;
		SendCacheUpdateNotification();
		m_pDumperThread->join();
		delete m_pDumperThread;
		m_pDumperThread = nullptr;
		return;
	};

	bool Message(const string &msg)
	{	
		if (!PushMessageToCache(msg))
			return false;
		SendCacheUpdateNotification();
		return true;
	};

	bool Message(const char* const msg)
	{
		return Message(string(msg));
	};

	template <typename T> bool Message(const T &t)
	{
		ostringstream oss;
		oss << t << endl;
		return Message(oss.str());
	};

	template <typename T> friend void DumperThreadProc(T *pLogger);

};


template <typename T> void DumperThreadProc(T *pLogger)
{
	while (true) {
		pLogger->WaitCacheUpdateNotification();

		string msg;
		while (pLogger->PopMessageFromCache(msg))
			pLogger->Dump(msg);

		if (pLogger->Done())
			break;
	}
	return;
};




#endif	/*LOGGER_H*/