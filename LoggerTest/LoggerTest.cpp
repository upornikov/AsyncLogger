#include "stdafx.h"

#include "logger\logger.h"
#include <gtest\gtest.h>

#include <iostream>     // cout, ostream, ios
#include <fstream>      // filebuf
#include <sstream>      // ostringstream

using namespace std;
static vector<string> strings = { "String 1", "String 2", "String 3", "String 4", "String 5" };
static string expected_output = "String 1\nString 2\nString 3\nString 4\nString 5\n";

TEST(SyncLoggingTest, StderrLogging) {
	testing::internal::CaptureStderr();
	{
		Logger<ostream> logger(cerr);
		for (auto s = strings.begin(); s != strings.end(); ++s)
			logger.Message(*s);
	}
	//logging is asyncronous - messages are committed for sure after Logger's destruction 
	ASSERT_EQ(expected_output, testing::internal::GetCapturedStderr());
};

TEST(SyncLoggingTest, StdoutLogging) {
	testing::internal::CaptureStdout();
	{
		Logger<ostream> logger(cout);
		for (auto s = strings.begin(); s != strings.end(); ++s)
			logger.Message(*s);
	}
	//logging is asyncronous - messages are committed for sure after Logger's destruction 
	ASSERT_EQ(expected_output, testing::internal::GetCapturedStdout());
};

TEST(SyncLoggingTest, StringStreamLogging) {
	ostringstream ss;
	{
		Logger<ostream> logger(ss);
		for (auto s = strings.begin(); s != strings.end(); ++s)
			logger.Message(*s);
	}
	//logging is asyncronous - messages are committed for sure after Logger's destruction 
	ASSERT_EQ(expected_output, ss.str());
};

TEST(SyncLoggingTest, FileLogging) {
	string logname = "SyncLoggingTest_FileLogging.txt";
	ofstream fs(logname);
	{
		Logger<ostream> logger(fs);
		for (auto s = strings.begin(); s != strings.end(); ++s)
			logger.Message(*s);
	}
	fs.close();
	//logging is asyncronous - messages are committed for sure after Logger's destruction
	ifstream fs1(logname);
	ostringstream ss;
	ss << fs1.rdbuf();
	fs1.close();
	remove(logname.c_str());
	ASSERT_EQ(expected_output, ss.str());
};

TEST(SyncLoggingTest, IntLogging) {
	vector<int> numbers = { -1, 0, 1024, 0xFFFF };
	stringstream ss;
	{
		Logger<ostream> logger(ss);
		for (auto i = numbers.begin(); i != numbers.end(); ++i)
			logger.Message(*i);
	}
	//logging is asyncronous - messages are committed for sure after Logger's destruction 
	int j;
	string line;
	vector<int> read_numbers;
	while (getline(ss, line)) {
		if (line.empty()) continue;
		istringstream iss(line);
		iss >> j;
		read_numbers.push_back(j);
	}
	ASSERT_EQ(numbers, read_numbers);
};

TEST(SyncLoggingTest, DoubleLogging) {
	vector<double> numbers = { double(-1), double(0.0), double(1024.777777), double(0xFFFF) };
	stringstream ss;
	{
		Logger<ostream> logger(ss);
		for (auto i = numbers.begin(); i != numbers.end(); ++i)
			logger.Message(*i);
	}
	//logging is asyncronous - messages are committed for sure after Logger's destruction 
	double j;
	string line;
	vector<double> read_numbers;
	while (getline(ss, line)) {
		if (line.empty()) continue;
		istringstream iss(line);
		iss >> j;
		read_numbers.push_back(j);
	}
	for (auto f1 = numbers.begin(), f2 = read_numbers.begin(); f1 != numbers.end(), f2 != read_numbers.end(); ++f1, ++f2)
		ASSERT_NEAR(*f1, *f2, 0.01);
};

TEST(SyncLoggingTest, FloatLogging) {
	vector<float> numbers = { -1.123f, 0.0f, 1024.777777f, (float)0xFFFF };
	stringstream ss;
	{
		Logger<ostream> logger(ss);
		for (auto i = numbers.begin(); i != numbers.end(); ++i)
			logger.Message(*i);
	}
	//logging is asyncronous - messages are committed for sure after Logger's destruction 
	float j;
	string line;
	vector<float> read_numbers;
	while (getline(ss, line)) {
		if (line.empty()) continue;
		istringstream iss(line);
		iss >> j;
		read_numbers.push_back(j);
	}
	for( auto f1=numbers.begin(), f2=read_numbers.begin(); f1 != numbers.end(), f2!=read_numbers.end(); ++f1, ++f2)
		ASSERT_NEAR(*f1, *f2, 0.01);
};


class AsyncLoggingTest : public testing::Test {
public:
	vector<thread*> workerThreadsPool;

	AsyncLoggingTest() {};

	void SetUp() {
	};

	void TearDown() {
		JoinWorkerThreads();
	};

	~AsyncLoggingTest() {};

	void JoinWorkerThreads() {
		for (auto t : workerThreadsPool)
		{
			t->join();
			delete t;
		}
		workerThreadsPool.clear();
	}
};

template <typename T> void logger_worker_thread(T *pLogger) {
	for (auto s = strings.begin(); s != strings.end(); ++s)
		pLogger->Message(*s);
};

//the number of threads to log from 
#define NUMBER_OF_LOGGING_THREADS 25
typedef Logger<stringstream> StringLogger;

TEST_F(AsyncLoggingTest, UnitTest1) {
	stringstream *pss = new stringstream();
	StringLogger *pLogger = new StringLogger(pss);	//this tests alternative Logger constructor

	for (int i = 0; i < NUMBER_OF_LOGGING_THREADS; ++i) {
		workerThreadsPool.push_back(new thread([=] { logger_worker_thread(pLogger); }));
	}
	//wait until all worker threads complete logging
	JoinWorkerThreads();
	//logging is asyncronous - messages are committed for sure after Logger's destruction
	delete pLogger;

	//get all logged strings into sorted string container, which allows duplicates
	string line;
	multiset<string> read_strings;
	while (getline(*pss, line)) {
		if (line.empty()) continue;
		read_strings.insert(line);
	}
	//make sure each string was correctly dumped once by every worker thread
	ASSERT_EQ(read_strings.size(), NUMBER_OF_LOGGING_THREADS*strings.size());
	for (auto s = strings.begin(); s != strings.end(); ++s)
		ASSERT_EQ(read_strings.count(*s), NUMBER_OF_LOGGING_THREADS);
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
